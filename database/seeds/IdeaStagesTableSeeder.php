<?php

use Illuminate\Database\Seeder;

class IdeaStagesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\IdeaStage::create(['name' => 'Idea']);
        \App\IdeaStage::create(['name' => 'MVP']);
        \App\IdeaStage::create(['name' => 'Launched']);
    }

}
