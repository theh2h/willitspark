<?php

use Illuminate\Database\Seeder;

class FeedbackTagsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\FeedbackTag::create(['name' => 'Design']);
        \App\FeedbackTag::create(['name' => 'Product']);
    }

}
