<?php

use Illuminate\Database\Seeder;

class IdeasTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idea = App\Idea::create([
                    'name' => 'DogHood',
                    's_description' => 'DogHood is a mobile application connecting you with other dogs and dog owners in your neighbourhood.',
                    'solution' => 'A mobile application that is GPS based. It will connect you with your neighbours who also own dogs to make your dog more social towards other dogs and other human beings.',
                    'logo_path' => 'uploads/images/ideas/logos/idea_1_logo.jpg',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([1, 2]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique'],
            3 => ['spark' => false, 'yes' => false, 'feedback' => null]
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_1_1.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_1_2.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_1_3.jpg']);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $idea = App\Idea::create([
                    'name' => 'Bucket.li',
                    's_description' => 'Meet new people that have the same elements of your bucket list.',
                    'solution' => 'A virtual social platform, connecting people with similar bucket lists, making it easier for them to meet and cross their items out together.',
                    'logo_path' => 'uploads/images/ideas/logos/idea_2_logo.jpg',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([3, 4]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique'],
            3 => ['spark' => false, 'yes' => false, 'feedback' => null]
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_2_1.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_2_2.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_2_3.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_2_4.jpg']);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $idea = App\Idea::create([
                    'name' => 'Fat-ract',
                    's_description' => 'Online Weight loss competition.',
                    'solution' => 'A web based competition where people that have overweight problems can apply.',
                    'logo_path' => 'uploads/images/ideas/logos/idea_3_logo.png',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([3, 4]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique'],
            3 => ['spark' => false, 'yes' => false, 'feedback' => null]
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_3_1.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_3_2.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_3_3.jpg']);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $idea = App\Idea::create([
                    'name' => 'JOB24',
                    's_description' => 'Job for one day for travellers',
                    'solution' => 'Web based solution for getting one day job offers for travellers',
                    'logo_path' => 'uploads/images/ideas/logos/idea_4_logo.png',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([3, 4]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique']
        ]);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $idea = App\Idea::create([
                    'name' => 'Playdare',
                    's_description' => 'Challenge and dare your friends',
                    'solution' => 'A web application for increasing social interaction in a challenging way. Dare your friends to do something and ask for proofs.',
                    'logo_path' => 'uploads/images/ideas/logos/idea_5_logo.jpg',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([3, 4]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the']
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_5_1.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_5_2.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_5_3.jpg']);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $idea = App\Idea::create([
                    'name' => 'Task Witch',
                    's_description' => 'Easily Collaborate and Engage',
                    'solution' => 'A dynamic task management tool. Where the team gets to communicate, collaborate and engage with their opinion about their organization and their work',
                    'logo_path' => 'uploads/images/ideas/logos/idea_6_logo.png',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([3, 4]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_6_1.png']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_6_2.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_6_3.jpg']);



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $idea = App\Idea::create([
                    'name' => 'Pizzaddict',
                    's_description' => 'Make your own pizza and wait for it to be delivered',
                    'solution' => ' A mobile /Tablet Application where you can make your own pizza from scratch and choose the ingredients that you want.',
                    'logo_path' => 'uploads/images/ideas/logos/idea_7_logo.png',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([1, 2]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique'],
            3 => ['spark' => false, 'yes' => false, 'feedback' => null]
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_7_1.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_7_2.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_7_3.jpg']);

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $idea = App\Idea::create([
                    'name' => 'Get Me That',
                    's_description' => 'Help people in your neighborhood that are unable to go out to buy things they need.',
                    'solution' => 'A Mobile application where people get to post their needs and someone that lives next to you can accept your request and buy you the things you need.',
                    'logo_path' => 'uploads/images/ideas/logos/idea_8_logo.png',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([1, 2]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique'],
            3 => ['spark' => false, 'yes' => false, 'feedback' => null]
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_8_1.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_8_2.jpg']);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $idea = App\Idea::create([
                    'name' => 'Let’sCom',
                    's_description' => 'Chat Rooms for people who want to improve their spoken languages..',
                    'solution' => ' A web application that combines people who want to enhance their ability to speak in a certain language. Topics will be interests based. So that people can find something interesting to talk about..',
                    'logo_path' => 'uploads/images/ideas/logos/idea_9_logo.png',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([1, 2]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique'],
            3 => ['spark' => false, 'yes' => false, 'feedback' => null]
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_9_1.jpg']);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $idea = App\Idea::create([
                    'name' => 'Never Give Up',
                    's_description' => 'Join other people that are going through the same challenges and motivate each other.',
                    'solution' => 'A web application where you can find someone who is going through the same challenge that you are going through. Share insights, motivate each other, share achievements.',
                    'logo_path' => 'uploads/images/ideas/logos/idea_10_logo.png',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([1, 2]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique'],
            3 => ['spark' => false, 'yes' => false, 'feedback' => null]
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_10_1.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_10_2.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_10_3.jpg']);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $idea = App\Idea::create([
                    'name' => 'Walk Map',
                    's_description' => ' Find out good places to walk in the city you’re in.',
                    'solution' => ' A GPS based mobile application that shows you where is the nearest good place to walk?.',
                    'logo_path' => 'uploads/images/ideas/logos/idea_11_logo.png',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([1, 2]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique'],
            3 => ['spark' => false, 'yes' => false, 'feedback' => null]
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_11_1.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_11_2.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_11_3.jpg']);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $idea = App\Idea::create([
                    'name' => 'WedEx',
                    's_description' => 'Plan your wedding from your phone.',
                    'problem' => ' People get married every day and it’s always a hassle to organize everything and make it perfect for the big day.',
                    'solution' => 'A mobile application that combines the wedding planning experts in your area. Includes: services, prices, time availability, floor plans….etc.',
                    'logo_path' => 'uploads/images/ideas/logos/idea_12_logo.png',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([1, 2]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique'],
            3 => ['spark' => false, 'yes' => false, 'feedback' => null]
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_12_1.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_12_2.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_12_3.jpg']);


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $idea = App\Idea::create([
                    'name' => 'Bandup',
                    's_description' => 'Connect with other musicians and form your band.',
                    'solution' => 'Many solo artists and music players are there in the world. But it is hard to find a group of people that play what you play, live nearby and interested in forming / joining a band..',
                    'logo_path' => 'uploads/images/ideas/logos/idea_13_logo.png',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([1, 2]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique'],
            3 => ['spark' => false, 'yes' => false, 'feedback' => null]
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_13_1.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_13_2.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_13_3.jpg']);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $idea = App\Idea::create([
                    'name' => 'Let’s Fish',
                    's_description' => ' Everything you need for fishing.',
                    'solution' => 'A web application for connecting people who have fishing as an interest, hobby or job.',
                    'logo_path' => 'uploads/images/ideas/logos/idea_14_logo.png',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([1, 2]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique'],
            3 => ['spark' => false, 'yes' => false, 'feedback' => null]
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_14_1.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_14_2.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_14_3.jpg']);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $idea = App\Idea::create([
                    'name' => 'Catch Up',
                    's_description' => ' Catch up with your friends and know who is free when ',
                    'solution' => 'A mobile application which is calendar based. Through which you can organize outings and invite your friends to check their time availability',
                    'logo_path' => 'uploads/images/ideas/logos/idea_15_logo.png',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([1, 2]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique'],
            3 => ['spark' => false, 'yes' => false, 'feedback' => null]
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_15_1.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_15_2.png']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_15_3.jpg']);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $idea = App\Idea::create([
                    'name' => 'What’s your name again?',
                    's_description' => ' A mobile application for remembering people’s names ',
                    'solution' => 'A mobile application that lets you create a mini contact list for new people you meet.',
                    'logo_path' => 'uploads/images/ideas/logos/idea_16_logo.png',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([1, 2]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique'],
            3 => ['spark' => false, 'yes' => false, 'feedback' => null]
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_16_1.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_16_2.jpg']);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $idea = App\Idea::create([
                    'name' => 'I am there',
                    's_description' => 'A mobile application that creates background noise simulating that you’re in a place that you choose.',
                    'solution' => 'A mobile application that lets you create background noises for places you choose (Party, street, queue, sea and more)',
                    'logo_path' => 'uploads/images/ideas/logos/idea_17_logo.png',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([1, 2]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique'],
            3 => ['spark' => false, 'yes' => false, 'feedback' => null]
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_17_1.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_17_2.png']);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $idea = App\Idea::create([
                    'name' => 'Get that rent',
                    's_description' => ' Rental bidding service ',
                    'solution' => 'A web application for bidding on basically anything that is for rent. Which provides a chance of getting it a little bit cheaper than usual',
                    'logo_path' => 'uploads/images/ideas/logos/idea_18_logo.png',
                    'featured' => 1,
                    'user_id' => 1
        ]);
        $idea->interests()->attach([1, 2]);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->views()->create(['ip' => '156.205.1.45']);
        $idea->sparks()->attach([
            1 => ['spark' => true, 'yes' => true, 'feedback' => 'I mean it sounds like a good idea but i would probably base it in a certain location so that I can ignore the'],
            2 => ['spark' => false, 'yes' => true, 'feedback' => 'To be completely honest I don’t think there is anything new about the idea , you need to find your unique'],
            3 => ['spark' => false, 'yes' => false, 'feedback' => null]
        ]);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_18_1.jpg']);
        $idea->photos()->create(['photo_path' => 'uploads/images/ideas/idea_18_2.jpg']);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

}
