<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
//        DB::statement("SET foreign_key_checks=0");
//        $this->call(InterestsTableSeeder::class);
//        $this->call(UsersTableSeeder::class);
//        $this->call(IdeasTableSeeder::class);
//        $this->call(FeedbackTagsTableSeeder::class);
        $this->call(IdeaStagesTableSeeder::class);





        Model::reguard();
//        DB::statement("SET foreign_key_checks=1");
    }

}
