<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
                    'email' => 'theh2h@gmail.com',
                    'f_name' => 'Omar',
                    'l_name' => 'Furrer',
        ]);
        $user->interests()->attach([1, 2, 3, 4, 5, 6]);

        $user = App\User::create([
                    'email' => 'omar.furrer@gmail.com',
                    'f_name' => 'Omar',
                    'l_name' => 'Ruedy',
        ]);
        $user->interests()->attach([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

        $user = App\User::create([
                    'email' => 'ahmed.algalladd@gmail.com',
                    'f_name' => 'Ahmed',
                    'l_name' => 'Gallad',
        ]);
        $user->interests()->attach([1, 2, 3, 4, 5, 6]);
    }

}
