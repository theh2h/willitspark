<?php

use App\Interest;
use Illuminate\Database\Seeder;

class InterestsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Interest::create(['name' => 'Books']);
        Interest::create(['name' => 'Business']);
        Interest::create(['name' => 'Catalogs']);
        Interest::create(['name' => 'Education']);
        Interest::create(['name' => 'Entertainment']);
        Interest::create(['name' => 'Finance']);
        Interest::create(['name' => 'Food & Drink']);
        Interest::create(['name' => 'Games']);
        Interest::create(['name' => 'Health & Fitness']);
        Interest::create(['name' => 'Lifestyle']);
        Interest::create(['name' => 'Medical']);
        Interest::create(['name' => 'Music']);
        Interest::create(['name' => 'Navigation']);
        Interest::create(['name' => 'News']);
        Interest::create(['name' => 'Photo & Video']);
        Interest::create(['name' => 'Productivity']);
        Interest::create(['name' => 'Reference']);
        Interest::create(['name' => 'Social Networking']);
        Interest::create(['name' => 'Sports']);
        Interest::create(['name' => 'Travel']);
        Interest::create(['name' => 'Utilities']);
        Interest::create(['name' => 'Weather']);
        Interest::create(['name' => 'Other']);
    }

}
