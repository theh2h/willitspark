<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStageToIdeasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ideas', function (Blueprint $table) {
            $table->integer('idea_stage_id')->unsigned()->index()->nullable()->after('logo_path');
            $table->foreign('idea_stage_id')->references('id')->on('idea_stages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ideas', function (Blueprint $table) {
            $table->dropForeign('ideas_idea_stage_id_foreign');
            $table->dropColumn('idea_stage_id');
        });
    }

}
