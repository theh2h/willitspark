<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSparksAndFeedbackFromSparksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sparks', function (Blueprint $table) {
            $table->dropColumn('spark');
            $table->dropColumn('feedback');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sparks', function (Blueprint $table) {
            $table->boolean('spark');
            $table->string('feedback', 500)->nullable();
        });
    }

}
