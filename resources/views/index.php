<!DOCTYPE html>
<html >
    <head>

        <meta charset="utf-8">
        <base href="<?php echo env('BASE_PATH', '/willitspark/public/') ?>">

        <!-- If you delete this meta tag World War Z will become a reality -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Will It Spark - Discover And Rate Amazing New Startups And Products</title>
        <meta name="description" content="Discover , Share And Rate Amazing New Startups And Products">
        <link rel="canonical" href="http://www.willitspark.com">

        <link rel="shortcut icon" type="image/png" href="assets/images/favicon.ico"/>

        <link rel="stylesheet" href="css/app.css?v=1.0">
        <link rel="stylesheet" href="css/pure/all.css?v=1.0">


<!--        <script>
            (function(i, s, o, g, r, a, m){i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function(){
            (i[r].q = i[r].q || []).push(arguments)}, i[r].l = 1 * new Date(); a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');</script>-->



    </head>
    <body ng-app="willitspark" ng-controller="BaseController as base">

    <ng-include  src="'js/angular/shared/navbar/_navbar.html'"></ng-include>

    <div class="main-wrapper">


        <div class='custom-main-content' ng-cloak >

            <div  flash-message="4000" class="custom-flash-alert"></div> 

            <div class="custom-ui-view"  ui-view></div>

        </div>    

    </div>

    <ng-include  src="'js/angular/shared/footer/_footer.html'"></ng-include>

    <angular-feedback options="base.feedbackOptions"></angular-feedback>

    <script src="js/app.js?v=1.0"></script>
    <script src="js/angular/app.js"></script>

    <!--base component-->
    <script src="js/angular/shared/controllers/BaseController.js"></script>
    <script src="js/angular/shared/ideas/IdeaController.js"></script>
    <script src="js/angular/shared/ideas/FeedbackBlockController.js"></script>
    <script src="js/angular/shared/ideas/FeedbackBoxController.js"></script>
    <script src="js/angular/components/home/HomeController.js"></script>
    <script src="js/angular/components/login/LoginController.js"></script>
    <script src="js/angular/components/browse/BrowseController.js"></script>
    <script src="js/angular/components/idea/ShowIdeaController.js"></script>
    <script src="js/angular/components/postNewIdea/PostNewIdeaController.js"></script>
    <script src="js/angular/components/my_ideas/MyIdeasController.js"></script>
    <script src="js/angular/components/my_favorites/MyFavoritesController.js"></script>
    <script src="js/angular/components/my_profile/MyProfileController.js"></script>
    <script src="js/angular/components/edit_idea/EditIdeaController.js"></script>
    <script src="js/angular/components/how_it_works/HowItWorksController.js"></script>



    <!--shared services-->
    <script src="js/angular/shared/services/BaseService.js"></script>
    <script src="js/angular/shared/services/UsersService.js"></script>
    <script src="js/angular/shared/services/IdeasService.js"></script>
    <script src="js/angular/shared/services/IdeaFeedbackService.js"></script>






</body>
</html>