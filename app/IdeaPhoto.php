<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdeaPhoto extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'idea_photos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['photo_path', 'idea_id'];

    /**
     * A product belongs to an idea
     *
     * @return BelongsTo
     */
    public function idea()
    {
        return $this->belongsTo('App\Idea');
    }

}
