<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'interests';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * A product can belong to many users
     *
     * @return BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'interest_user')->withTimestamps();
    }

    /**
     * A product can belong to many users
     *
     * @return BelongsToMany
     */
    public function ideas()
    {
        return $this->belongsToMany('App\Idea', 'idea_interest')->withTimestamps();
    }

}
