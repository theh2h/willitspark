<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdeaComment extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'idea_comments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['text', 'user_id', 'idea_id'];

    /**
     * A product belongs to a user
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * A product belongs to a idea
     *
     * @return BelongsTo
     */
    public function idea()
    {
        return $this->belongsTo('App\Idea');
    }

}
