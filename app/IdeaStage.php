<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdeaStage extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'idea_stages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

}
