<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedbackTag extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'feedback_tags';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * A product has many questions
     *
     * @return HasMany
     */
    public function feedback()
    {
        return $this->hasMany('App\IdeaFeedback');
    }

}
