<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdeaView extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'idea_views';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ip'];

}
