<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable,
        CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'f_name', 'l_name', 'photo_path', 'super_admin', 'linkedin_id', 'last_seen'];

    /**
     * A product has many ideas
     *
     * @return HasMany
     */
    public function ideas()
    {
        return $this->hasMany('App\Idea');
    }

    /**
     * A product can belong to many users
     *
     * @return BelongsToMany
     */
    public function favorited_ideas()
    {
        return $this->belongsToMany('App\Idea', 'favorited_ideas')->withTimestamps();
    }

    /**
     * A product can belong to many users
     *
     * @return BelongsToMany
     */
    public function interests()
    {
        return $this->belongsToMany('App\Interest', 'interest_user')->withTimestamps();
    }

    /**
     * A product can belong to many users
     *
     * @return BelongsToMany
     */
    public function sparks()
    {
        return $this->belongsToMany('App\Idea', 'sparks')->withPivot('yes', 'spark', 'feedback')->withTimestamps();
    }

    /**
     * Return user if exists; create and return if doesn't
     */
    public static function findOrCreateUserLinkedin($user, $provider)
    {
        $existingUser = User::where($provider . '_id', $user->id)->first();
        if ($existingUser) {
            $existingUser->f_name = $user->user['firstName'];
            $existingUser->l_name = $user->user['lastName'];
            $existingUser->headline = $user->user['headline'];
            $existingUser->email = $user->email;
            $existingUser->photo_path = $user->avatar;
            $existingUser->attributes[$provider . '_id'] = $user->id;
            $existingUser->save();
            return $existingUser;
        } else {
            $existingUser = User::where('email', $user->email)->first();
            if ($existingUser) {
                $existingUser->f_name = $user->user['firstName'];
                $existingUser->l_name = $user->user['lastName'];
                $existingUser->headline = $user->user['headline'];
                $existingUser->email = $user->email;
                $existingUser->photo_path = $user->avatar;
                $existingUser->attributes[$provider . '_id'] = $user->id;
                $existingUser->save();
                return $existingUser;
            }
            $newUser = new User;
            $newUser->f_name = $user->user['firstName'];
            $newUser->l_name = $user->user['lastName'];
            $newUser->headline = $user->user['headline'];
            $newUser->email = $user->email;
            $newUser->photo_path = $user->avatar;
            $newUser->attributes[$provider . '_id'] = $user->id;
            $newUser->save();
            return $newUser;
        }
    }

    /**
     * Return user if exists; create and return if doesn't
     */
    public static function findOrCreateUserGoogle($user, $provider)
    {
        $existingUser = User::where($provider . '_id', $user->id)->first();
        if ($existingUser) {
            $existingUser->f_name = $user->user['name']['givenName'];
            $existingUser->l_name = $user->user['name']['familyName'];
            $existingUser->email = $user->email;
            $existingUser->photo_path = $user->avatar;
            $existingUser->attributes[$provider . '_id'] = $user->id;
            $existingUser->save();
            return $existingUser;
        } else {
            $existingUser = User::where('email', $user->email)->first();
            if ($existingUser) {
                $existingUser->f_name = $user->user['name']['givenName'];
                $existingUser->l_name = $user->user['name']['familyName'];
                $existingUser->email = $user->email;
                $existingUser->photo_path = $user->avatar;
                $existingUser->attributes[$provider . '_id'] = $user->id;
                $existingUser->save();
                return $existingUser;
            }
            $newUser = new User;
            $newUser->f_name = $user->user['name']['givenName'];
            $newUser->l_name = $user->user['name']['familyName'];
            $newUser->email = $user->email;
            $newUser->photo_path = $user->avatar;
            $newUser->attributes[$provider . '_id'] = $user->id;
            $newUser->save();
            return $newUser;
        }
    }

}
