<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Idea;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Image;
use SebastianBergmann\RecursionContext\Exception;

class IdeasController extends Controller {

    public function __construct()
    {
        $this->middleware('jwt.auth', ['only' => ['getMyFavorites', 'getMyIdeas', 'getHeart', 'getUnheart', 'postYes', 'postNo', 'deleteYesNo', 'store', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $ideas = Idea::all();



            return response()->json(compact('ideas'), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function getMyFavorites()
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();

            $ideas = $user->favorited_ideas()->orderBy('created_at', 'DESC')->get();

            foreach ($ideas as $key => $idea) {
                $ideas[$key]['favorited'] = true;
                $ideas[$key]['views_count'] = $idea->views()->count();
                $ideas[$key]['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
                $ideas[$key]['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();
            }

            return response()->json(compact('ideas'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function getMyIdeas()
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();

            $ideas = $user->ideas()->orderBy('created_at', 'DESC')->get();
            foreach ($ideas as $key => $idea) {
                $favorited = $user->favorited_ideas()->where('favorited_ideas.idea_id', $idea->id)->exists();
                $ideas[$key]['favorited'] = $favorited;
                $ideas[$key]['views_count'] = $idea->views()->count();
                $ideas[$key]['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
                $ideas[$key]['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();
            }

            return response()->json(compact('ideas'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function getFeatured()
    {
        try {

            $ideas = Idea::where('featured', true)->orderBy('id', 'DESC')->get();

            try {
                $user = JWTAuth::parseToken()->authenticate();
                foreach ($ideas as $key => $idea) {
                    $favorited = $user->favorited_ideas()->where('favorited_ideas.idea_id', $idea->id)->exists();
                    $ideas[$key]['favorited'] = $favorited;
                    $ideas[$key]['views_count'] = $idea->views()->count();
                    $ideas[$key]['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
                    $ideas[$key]['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();
                }
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $ex) {
                foreach ($ideas as $key => $idea) {
                    $ideas[$key]['favorited'] = false;
                    $ideas[$key]['views_count'] = $idea->views()->count();
                    $ideas[$key]['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
                    $ideas[$key]['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();
                }
            }

            return response()->json(compact('ideas'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function getTop($page = 1)
    {
        try {

            $ideas = Idea::join('yeses', 'ideas.id', '=', 'yeses.idea_id')
                    ->groupBy('ideas.id')
                    ->select(
                            'ideas.*', DB::raw('sum(case when yeses.yes = 1 then 1 else 0 end) as yes_count')
                    )
                    ->orderBy('yes_count', 'DESC')
                    ->paginate(8);



            try {
                $user = JWTAuth::parseToken()->authenticate();
                foreach ($ideas as $key => $idea) {
                    $favorited = $user->favorited_ideas()->where('favorited_ideas.idea_id', $idea->id)->exists();
                    $ideas[$key]['favorited'] = $favorited;
                    $ideas[$key]['views_count'] = $idea->views()->count();
                    $ideas[$key]['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
                    $ideas[$key]['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();
                }
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $ex) {
                foreach ($ideas as $key => $idea) {
                    $ideas[$key]['favorited'] = false;
                    $ideas[$key]['views_count'] = $idea->views()->count();
                    $ideas[$key]['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
                    $ideas[$key]['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();
                }
            }
            $ideas = $ideas->toArray();

            return response()->json(compact('ideas'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function getViews($page = 1)
    {
        try {

            $ideas = Idea::leftJoin('idea_views', 'ideas.id', '=', 'idea_views.idea_id')
                    ->groupBy('ideas.id')
                    ->select(
                            'ideas.*', DB::raw('count(idea_views.id) as views_count')
                    )
                    ->orderBy('views_count', 'DESC')
                    ->paginate(8);



            try {
                $user = JWTAuth::parseToken()->authenticate();
                foreach ($ideas as $key => $idea) {
                    $favorited = $user->favorited_ideas()->where('favorited_ideas.idea_id', $idea->id)->exists();
                    $ideas[$key]['favorited'] = $favorited;
                    $ideas[$key]['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
                    $ideas[$key]['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();
                }
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $ex) {
                foreach ($ideas as $key => $idea) {
                    $ideas[$key]['favorited'] = false;
                    $ideas[$key]['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
                    $ideas[$key]['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();
                }
            }
            $ideas = $ideas->toArray();

            return response()->json(compact('ideas'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function getRecent($page = 1)
    {
        try {

            $ideas = Idea::orderBy('created_at', 'DESC')->paginate(8);




            try {
                $user = JWTAuth::parseToken()->authenticate();
                foreach ($ideas as $key => $idea) {
                    $favorited = $user->favorited_ideas()->where('favorited_ideas.idea_id', $idea->id)->exists();
                    $ideas[$key]['favorited'] = $favorited;
                    $ideas[$key]['views_count'] = $idea->views()->count();
                    $ideas[$key]['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
                    $ideas[$key]['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();
                }
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $ex) {
                foreach ($ideas as $key => $idea) {
                    $ideas[$key]['favorited'] = false;
                    $ideas[$key]['views_count'] = $idea->views()->count();
                    $ideas[$key]['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
                    $ideas[$key]['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();
                }
            }

            $ideas = $ideas->toArray();

            return response()->json(compact('ideas'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function getOthers(Idea $idea)
    {
        try {

            $ideas = Idea::all()->shuffle()->slice(0, 3);

            try {
                $user = JWTAuth::parseToken()->authenticate();
                foreach ($ideas as $key => $other) {
                    $favorited = $user->favorited_ideas()->where('favorited_ideas.idea_id', $other->id)->exists();
                    $ideas[$key]['favorited'] = $favorited;
                    $ideas[$key]['views_count'] = $other->views()->count();
                    $ideas[$key]['yes_count'] = DB::table('yeses')->where('idea_id', $other->id)->where('yes', 1)->count();
                    $ideas[$key]['no_count'] = DB::table('yeses')->where('idea_id', $other->id)->where('yes', 0)->count();
                }
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $ex) {
                foreach ($ideas as $key => $other) {
                    $ideas[$key]['favorited'] = false;
                    $ideas[$key]['views_count'] = $other->views()->count();
                    $ideas[$key]['yes_count'] = DB::table('yeses')->where('idea_id', $other->id)->where('yes', 1)->count();
                    $ideas[$key]['no_count'] = DB::table('yeses')->where('idea_id', $other->id)->where('yes', 0)->count();
                }
            }

            return response()->json(compact('ideas'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function getHeart(Idea $idea)
    {
        try {
            $idea->load('interests');

            $user = JWTAuth::parseToken()->authenticate();
            $user->favorited_ideas()->attach($idea);


            $idea['views_count'] = $idea->views()->count();
            $idea['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
            $idea['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();

            $idea['favorited'] = true;

            $yes = $idea->yeses()->where('user_id', $user->id)->first();
            if ($yes) {
                $idea['yes'] = $yes->pivot->yes;
            } else {
                $idea['yes'] = null;
            }

            return response()->json(compact('idea'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function getUnheart(Idea $idea)
    {
        try {
            $idea->load('interests');

            $user = JWTAuth::parseToken()->authenticate();
            $user->favorited_ideas()->detach($idea);
            $idea['views_count'] = $idea->views()->count();
            $idea['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
            $idea['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();

            $idea['favorited'] = false;
            $yes = $idea->yeses()->where('user_id', $user->id)->first();
            if ($yes) {
                $idea['yes'] = $yes->pivot->yes;
            } else {
                $idea['yes'] = null;
            }

            return response()->json(compact('idea'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function getYes(Idea $idea)
    {
        try {

            $user = JWTAuth::parseToken()->authenticate();
            $idea->yeses()->attach([ $user->id => ['yes' => true]]);

            $idea->load('yeses', 'interests');

            $favorited = $user->favorited_ideas()->where('favorited_ideas.idea_id', $idea->id)->exists();
            $idea['favorited'] = $favorited;
            $idea['views_count'] = $idea->views()->count();
            $idea['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
            $idea['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();
            $idea['yes'] = true;


            return response()->json(compact('idea'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function getNo(Idea $idea)
    {
        try {

            $user = JWTAuth::parseToken()->authenticate();
            $idea->yeses()->attach([ $user->id => ['yes' => false]]);

            $idea->load('interests');

            $favorited = $user->favorited_ideas()->where('favorited_ideas.idea_id', $idea->id)->exists();
            $idea['favorited'] = $favorited;
            $idea['views_count'] = $idea->views()->count();
            $idea['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
            $idea['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();
            $idea['yes'] = false;


            return response()->json(compact('idea'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function deleteYesNo(Idea $idea)
    {
        try {

            $user = JWTAuth ::parseToken()->authenticate();
            $idea->yeses()->detach($user->id);
            $idea->load('interests');
            $favorited = $user->favorited_ideas()->where('favorited_ideas.idea_id', $idea->id)->exists();
            $idea['favorited'] = $favorited;
            $idea['views_count'] = $idea->views()->count();
            $idea['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
            $idea['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();
            $idea['yes'] = null;

            return response()->json(compact('idea'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {


            $tags = \App\Interest::all();
            $stages = \App\IdeaStage::all();


            return response()->json(compact('tags', 'stages'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\StoreIdeaRequest $request)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();

            $idea = $user->ideas()->create($request->all());
            $idea->interests()->attach($request->tags);



            $file = $request->file('file');
            $destinationPathMap = 'uploads/images/ideas/logos';
            if ($file) {
                $original_extension = $file->getClientOriginalExtension();
                $new_file_name = str_replace(' ', '', str_random(30) . '.' . $original_extension);
                $save_proccess = $file->move($destinationPathMap, $new_file_name);
                if ($save_proccess) {
                    $idea->logo_path = $new_file_name;
                    $idea->save();
                    Image::make($destinationPathMap . '/' . $idea->logo_path)->resize(290, 230)->save($destinationPathMap . '/thumbnail_small/' . $new_file_name, 70);
                    Image::make($destinationPathMap . '/' . $idea->logo_path)->resize(490, 230)->save($destinationPathMap . '/thumbnail_medium/' . $new_file_name, 70);
                    Image::make($destinationPathMap . '/' . $idea->logo_path)->resize(1135, 450)->save($destinationPathMap . '/thumbnail_large/' . $new_file_name, 100);
                }
            }

            return response()->json(compact('idea'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Idea $idea)
    {
        try {
            $idea->views()->create(['ip' => \Request::ip()]);
            $idea->load('interests', 'stage');

            try {
                $user = JWTAuth::parseToken()->authenticate();
                $favorited = $user->favorited_ideas()->where('favorited_ideas.idea_id', $idea->id)->exists();
                $idea['favorited'] = $favorited;
                $yes = $idea->yeses()->where('user_id', $user->id)->first();
                if ($yes) {
                    $idea['yes'] = $yes->pivot->yes;
                } else {
                    $idea['yes'] = null;
                }
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $ex) {
                $idea['favorited'] = false;
                $idea['yes'] = null;
            }

            $idea['views_count'] = $idea->views()->count();
            $idea['yes_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 1)->count();
            $idea['no_count'] = DB::table('yeses')->where('idea_id', $idea->id)->where('yes', 0)->count();




            return response()->json(compact('idea'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Idea $idea)
    {
        try {

            $idea->load('interests', 'stage');
            $tags = \App\Interest::all();
            $stages = \App\IdeaStage::all();

            return response()->json(compact('idea', 'tags', 'stages'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\UpdateIdeaRequest $request, Idea $idea)
    {

        try {
            $user = JWTAuth::parseToken()->authenticate();

            $idea->update($request->all());
            if ($request->tags) {
                $idea->interests()->sync($request->tags);
            } else {
                $idea->interests()->delete();
            }



            $file = $request->file('file');
            $destinationPathMap = 'uploads/images/ideas/logos';
            if ($file) {
                $original_extension = $file->getClientOriginalExtension();
                $new_file_name = str_replace(' ', '', str_random(30) . '.' . $original_extension);
                $save_proccess = $file->move($destinationPathMap, $new_file_name);
                if ($idea->logo_path != null) {
                    unlink($destinationPathMap . '/' . $idea->logo_path);
                    unlink($destinationPathMap . '/thumbnail_small/' . $idea->logo_path);
                    unlink($destinationPathMap . '/thumbnail_medium/' . $idea->logo_path);
                    unlink($destinationPathMap . '/thumbnail_large/' . $idea->logo_path);
                }
                if ($save_proccess) {
                    $idea->logo_path = $new_file_name;
                    $idea->save();
                    Image::make($destinationPathMap . '/' . $idea->logo_path)->resize(290, 230)->save($destinationPathMap . '/thumbnail_small/' . $new_file_name, 70);
                    Image::make($destinationPathMap . '/' . $idea->logo_path)->resize(490, 230)->save($destinationPathMap . '/thumbnail_medium/' . $new_file_name, 70);
                    Image::make($destinationPathMap . '/' . $idea->logo_path)->resize(1135, 450)->save($destinationPathMap . '/thumbnail_large/' . $new_file_name, 100);
                }
            }

            return response()->json(compact('idea'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
