<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Laravel\Socialite\Facades\Socialite;

class UsersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        try {




            return response()->json(compact('user'), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getMyProfile()
    {
        try {

            $user = JWTAuth::parseToken()->authenticate();
            $ideasCount = $user->ideas()->count();
            $sparksGiven = $user->sparks()->wherePivot('spark', 1)->count();
            $sparksAcquired = DB::table('sparks')->join('ideas', 'sparks.idea_id', '=', 'ideas.id')->where('sparks.spark', 1)->where('ideas.user_id', $user->id)->count();
            $user['ideasCount'] = $ideasCount;
            $user['sparksGiven'] = $sparksGiven;
            $user['sparksAcquired'] = $sparksAcquired;

            return response()->json(compact('user'), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function postAuthenticateLinkedin(Request $request)
    {
        try {
            $provider = 'linkedin';

            $user = Socialite::driver($provider)->stateless()->user();

            $user = User::findOrCreateUserLinkedin($user, $provider);

            $user->last_seen = date('Y-m-d H:i:s');
            $user->save();

            $token = JWTAuth::fromUser($user);

            return response()->json(compact('user', 'token'), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function postAuthenticateGoogle(Request $request)
    {
        try {
            $provider = 'google';

            $user = Socialite::driver($provider)->stateless()->user();

            $user = User::findOrCreateUserGoogle($user, $provider);

            $user->last_seen = date('Y-m-d H:i:s');
            $user->save();

            $token = JWTAuth::fromUser($user);

            return response()->json(compact('user', 'token'), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

}
