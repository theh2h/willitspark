<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Idea;
use JWTAuth;

class IdeaFeedbackController extends Controller {

    public function __construct()
    {
        $this->middleware('jwt.auth', ['only' => ['postRate', 'deleteRate', 'postFeedback']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexByTags(Idea $idea)
    {
        try {

            $tags = \App\FeedbackTag::all();
            foreach ($tags as $key => $tag) {
                $query = $tag->feedback()->where('idea_id', $idea->id)->where('user_id', '!=', $idea->user->id);
                $count = $query->count();
                $tags[$key]['count'] = $count;
                $average = $query->avg('rating');
                $tags[$key]['average'] = $average;
                $tags[$key]['percentage'] = ( $average / 5 ) * 100;
            }


            try {
                $user = JWTAuth::parseToken()->authenticate();
                foreach ($tags as $key => $tag) {
                    $record = $tag->feedback()->where('idea_id', $idea->id)->where('user_id', $user->id)->first();
                    if ($record) {
                        $tags[$key]['user_rating'] = $record->rating;
                        $tags[$key]['user_rated'] = true;
                    } else {
                        $tags[$key]['user_rating'] = null;
                        $tags[$key]['user_rated'] = false;
                    }
                }
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $ex) {
                foreach ($tags as $key => $tag) {
                    $tags[$key]['user_rating'] = null;
                    $tags[$key]['user_rated'] = false;
                }
            }

            return response()->json(compact('tags'), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Idea $idea)
    {
        try {

//            $feedback = \App\IdeaFeedback::where('idea_id', $idea->id)->with('user', 'tag')->get();
            $feedback = \App\IdeaFeedback::where('idea_id', $idea->id)->with('user', 'tag')->whereNotNull('feedback')->get();

            return response()->json(compact('feedback'), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function postRate(Idea $idea, \App\FeedbackTag $tag)
    {
        try {

            $user = JWTAuth::parseToken()->authenticate();

            $feedback = new \App\IdeaFeedback;
            $feedback->rating = Request::input('rating');
            $feedback->idea_id = $idea->id;
            $feedback->feedback_tag_id = $tag->id;
            $feedback->user_id = $user->id;
            $feedback->save();

            return response()->json(compact('feedback'), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function deleteRate(Idea $idea, \App\FeedbackTag $tag)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();

            $record = \App\IdeaFeedback::where('idea_id', $idea->id)->where('feedback_tag_id', $tag->id)->where('user_id', $user->id)->first();
            $record->delete();

            return response()->json(compact(''), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response2
     */
    public function postFeedback(Idea $idea, \App\FeedbackTag $tag)
    {
        try {

            $user = JWTAuth::parseToken()->authenticate();

            $feedback = \App\IdeaFeedback::where('idea_id', $idea->id)->where('feedback_tag_id', $tag->id)->where('user_id', $user->id)->first();
            $feedback->feedback = Request::input('feedback');
            $feedback->save();

            return response()->json(compact('feedback'), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {


            $tags = \App\FeedbackTag::all();

            return response()->json(compact('tags'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreIdeaFeedbackRequest $request, \App\Idea $idea)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $feedback = new \App\IdeaFeedback;
            $feedback->rating = $request->rating;
            $feedback->feedback = $request->feedback;
            $feedback->user_id = $user->id;
            $feedback->idea_id = $idea->id;
            $feedback->feedback_tag_id = $request->feedback_tag_id;
            $feedback->save();
            return response()->json(compact('feedback'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
