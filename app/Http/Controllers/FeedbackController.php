<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;

class FeedbackController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreFeedbackRequest $request)
    {
        try {


            $feedback = new \App\Feedback;
            $feedback->url = $request->feedback['url'];
            $feedback->text = $request->feedback['note'];
            $feedback->feedback = json_encode($request->feedback);
            $image = base64_decode(explode(',', $request->feedback['img'])[1]);
            $destinationPathMap = 'uploads/images/feedback';
            $name = $destinationPathMap . '/' . str_random(30) . '.png';
            $save_proccess = file_put_contents($name, $image);
            if ($save_proccess != false) {
                $feedback->photo_path = $name;
                $feedback->save();
            }

            try {
                $user = JWTAuth::parseToken()->authenticate();
                $feedback->user_id = $user->id;
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $ex) {
                
            }

            $feedback->save();

            return response()->json(compact('feedback'), 200);
        } catch (Exception $e) {
            return response()->json([ 'error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json([ 'error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
