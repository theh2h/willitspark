<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
Route::get('/', ['uses' => 'BaseController@index']);



Route::group(['prefix' => 'api'], function() {
    Route::post('users/authenticate/google', ['uses' => 'UsersController@postAuthenticateGoogle']);
    Route::post('users/authenticate/linkedin', ['uses' => 'UsersController@postAuthenticateLinkedin']);
    Route::get('users/my/profile', ['uses' => 'UsersController@getMyProfile']);
    Route::resource('users', 'UsersController');

    Route::delete('ideas/{ideas}/feedback/tags/{feedback_tags}/rate', ['uses' => 'IdeaFeedbackController@deleteRate']);
    Route::post('ideas/{ideas}/feedback/tags/{feedback_tags}/rate', ['uses' => 'IdeaFeedbackController@postRate']);
    Route::post('ideas/{ideas}/feedback/tags/{feedback_tags}/feedback', ['uses' => 'IdeaFeedbackController@postFeedback']);
    Route::get('ideas/{ideas}/feedback/tags', ['uses' => 'IdeaFeedbackController@indexByTags']);
    Route::resource('ideas.feedback', 'IdeaFeedbackController');

    Route::get('ideas/featured', ['uses' => 'IdeasController@getFeatured']);
    Route::get('ideas/top/{page?}', ['uses' => 'IdeasController@getTop']);
    Route::get('ideas/views/{page?}', ['uses' => 'IdeasController@getViews']);
    Route::get('ideas/recent/{page?}', ['uses' => 'IdeasController@getRecent']);
    Route::get('ideas/my/favorites', ['uses' => 'IdeasController@getMyFavorites']);
    Route::get('ideas/my/ideas', ['uses' => 'IdeasController@getMyIdeas']);
    Route::get('ideas/{ideas}/others', ['uses' => 'IdeasController@getOthers']);
    Route::get('ideas/{ideas}/heart', ['uses' => 'IdeasController@getHeart']);
    Route::get('ideas/{ideas}/unheart', ['uses' => 'IdeasController@getUnheart']);
    Route::get('ideas/{ideas}/yes', ['uses' => 'IdeasController@getYes']);
    Route::get('ideas/{ideas}/no', ['uses' => 'IdeasController@getNo']);
    Route::delete('ideas/{ideas}/undoyesno', ['uses' => 'IdeasController@deleteYesNo']);
    Route::resource('ideas', 'IdeasController');

    Route::resource('feedback', 'FeedbackController');
});

Route::any('{all}', function() {
    return view('index');
})->where('all', '.+');



