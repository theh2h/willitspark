<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Storage;

class SyncImages extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:sync {table : The table to sync with} {field : The table filed name} {directory : The directory to sync with}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync files with table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $table = $this->argument('table');
        $field = $this->argument('field');
        $directory = $this->argument('directory');

        $files = Storage::disk('public')->files($directory);
        $count = count($files);
        $counter = 0;
        foreach ($files as $key => $file) {
            $this->info($file);
            $counter++;
            $fileName = pathinfo($file)['basename'];
            $filePath = public_path() . '/' . $file;

            $record = DB::table($table)->where($field, $fileName)->first();

            if (!$record) {
                unlink($filePath);
                $this->info($counter . ' deleted');
            }
            $this->info($counter . ' of ' . $count . ' done');
        }
    }

}
