<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Storage;

class RenameImages extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:rename {model : The model to sync with} {table : The table to sync with} {field : The table filed name} {directory : The directory to sync with}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rename all images in the directory and sync the names with the table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model = $this->argument('model');
        $table = $this->argument('table');
        $field = $this->argument('field');
        $directory = $this->argument('directory');

        $NamespacedModel = '\\App\\' . $model;
        $records = $NamespacedModel::get();

        foreach ($records as $key => $record) {
            if ($record->logo_path != null) {

                $this->info('start');
                $pathExploded = explode(".", $record->logo_path);
                $mimeType = $pathExploded[count($pathExploded) - 1];
                $newName = str_random(30) . '.' . $mimeType;
                $newPath = $directory . '/' . $newName;
                $this->info($record->id);
                $storage = Storage::disk('public')->move($record->logo_path, $newPath);
                if ($storage) {
                    $record->logo_path = $newName;
                    $record->save();
                    $this->info('done');
                }
            }
        }
    }

}
