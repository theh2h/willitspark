<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;
use Image;

class OptimizeImages extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:optimize {from : The directory from where to get images} {to : The directory to where the images will be copied} {width : The new width of the images} {height : The new width of the images} {quality : The new quality of the images}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Optimize Images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $from = $this->argument('from');
        $to = $this->argument('to');
        $width = $this->argument('width');
        $height = $this->argument('height');
        $quality = $this->argument('quality');

        $files = Storage::disk('public')->files($from);
        $count = count($files);
        $counter = 0;
        foreach ($files as $key => $file) {
            $counter++;
            $fileName = pathinfo($file)['basename'];
            $filePath = public_path() . '/' . $file;
            $savePath = public_path() . '/' . $to . '/' . $fileName;

            $image = Image::make($filePath)->resize($width, $height)->save($savePath, $quality);
            if ($image) {
                $this->info('Image ' . $counter . ' of ' . $count . ' saved');
            }
        }
    }

}
