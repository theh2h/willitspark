<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdeaFeedback extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'idea_feedback';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['rating', 'feedback'];

    /**
     * A product belongs to a therapy area
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * A product belongs to a therapy area
     *
     * @return BelongsTo
     */
    public function tag()
    {
        return $this->belongsTo('App\FeedbackTag', 'feedback_tag_id');
    }

}
