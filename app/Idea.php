<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idea extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ideas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 's_description', 'solution', 'logo_path', 'idea_stage_id', 'url', 'featured', 'user_id'];

    /**
     * A product has many photos
     *
     * @return HasMany
     */
    public function photos()
    {
        return $this->hasMany('App\IdeaPhoto');
    }

    /**
     * A product has many comments
     *
     * @return HasMany
     */
    public function comments()
    {
        return $this->hasMany('App\IdeaComment');
    }

    /**
     * A product can belong to many users who sparked it
     *
     * @return BelongsToMany
     */
    public function interests()
    {
        return $this->belongsToMany('App\Interest', 'idea_interest')->withTimestamps();
    }

    /**
     * A product can belong to many users who sparked it
     *
     * @return BelongsToMany
     */
    public function yeses()
    {
        return $this->belongsToMany('App\User', 'yeses')->withPivot('yes')->withTimestamps();
    }

    /**
     * A product can belong to many users who sparked it
     *
     * @return BelongsToMany
     */
    public function users_favorited()
    {
        return $this->belongsToMany('App\User', 'favorited_ideas')->withTimestamps();
    }

    /**
     * A product has many views
     *
     * @return HasMany
     */
    public function views()
    {
        return $this->hasMany('App\IdeaView');
    }

    /**
     * A product has many feedback
     *
     * @return HasMany
     */
    public function feedback()
    {
        return $this->hasMany('App\IdeaFeedback');
    }

    /**
     * A product belongs to a User
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * A product belongs to a User
     *
     * @return BelongsTo
     */
    public function stage()
    {
        return $this->belongsTo('App\IdeaStage', 'idea_stage_id');
    }

}
