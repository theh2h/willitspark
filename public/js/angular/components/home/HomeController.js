(function () {

    'use strict';

    angular.module('willitspark').controller('HomeController', HomeController);

    function HomeController($auth, $state, $stateParams, $rootScope, BaseService, UsersService, IdeasService, $scope) {

        var vm = this;
        vm.ideas = [];
        getFeatured();

        $scope.$watch(function () {
            return IdeasService.ideas;
        },
                function (newValue, oldValue) {
                    vm.ideas = angular.copy(IdeasService.ideas);
                }, true);

        function getFeatured() {
            IdeasService.getFeatured();
        }





    }

})();