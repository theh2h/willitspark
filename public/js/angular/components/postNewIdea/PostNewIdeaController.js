(function () {

    'use strict';

    angular.module('willitspark').controller('PostNewIdeaController', PostNewIdeaController);

    function PostNewIdeaController($state, IdeasService, $rootScope, $uibModal) {

        var vm = this;
        vm.idea = {
            logo: null,
            name: '',
            s_description: '',
            solution: '',
            tags: [],
            idea_stage_id: 3
        };
        vm.tags = [];
        vm.stages = [];
        vm.uploading = false;
        vm.error = {};

        create();

        vm.store = function (idea) {
            if (!$rootScope.authenticated) {
                vm.openLogin();
            } else {
                vm.uploading = true;
                IdeasService.store(idea).then(function (data) {
                    $state.go('show', {id: data.data.idea.id});
                });
            }

        };

        vm.openLogin = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'js/angular/components/login/login.html',
                controller: 'LoginController',
                controllerAs: 'LoginCtrl',
                backdrop: true,
                windowClass: 'login-modal'
            });

        };

        function create() {
            IdeasService.create().then(function () {
                vm.tags = angular.copy(IdeasService.createObj.tags);
                vm.stages = angular.copy(IdeasService.createObj.stages);
            });
        }




    }

})();