(function () {

    'use strict';

    angular.module('willitspark').controller('BrowseController', BrowseController);

    function BrowseController($auth, $state, $stateParams, $rootScope, BaseService, UsersService, IdeasService, $scope) {

        var vm = this;
        vm.ideas = [];
        vm.customTitle = '';
        vm.page = 1;
        vm.disableShowMore = true;
        vm.buttonMessage = 'Loading ...';

        $scope.$watch(function () {
            return IdeasService.ideas;
        },
                function (newValue, oldValue) {
                    syncIdeas();
                }, true);


        vm.loadMore = function () {
            vm.page++;
            switch (vm.customTitle)
            {
                case "Top Rated":
                    vm.getTop();
                    break;
                case "Most Viewed":
                    vm.getViews();
                    break;
                case "Latest":
                    vm.getRecent();
                    break;

            }
        };

        vm.getTop = function () {
            if (vm.customTitle !== 'Top Rated') {
                vm.page = 1;
            }
            vm.customTitle = 'Top Rated';
            vm.disableShowMore = true;
            vm.buttonMessage = 'Loading ...';
            IdeasService.getTop(vm.page).then(function () {
                vm.disableShowMore = false;
                vm.buttonMessage = 'Load More';
            });
        };

        vm.getViews = function () {
            if (vm.customTitle !== 'Most Viewed') {
                vm.page = 1;
            }
            vm.customTitle = 'Most Viewed';
            vm.disableShowMore = true;
            vm.buttonMessage = 'Loading ...';
            IdeasService.getViews(vm.page).then(function () {
                vm.disableShowMore = false;
                vm.buttonMessage = 'Load More';
            });
        };

        vm.getRecent = function () {
            if (vm.customTitle !== 'Latest') {
                vm.page = 1;
            }
            vm.customTitle = 'Latest';
            vm.disableShowMore = true;
            vm.buttonMessage = 'Loading ...';
            IdeasService.getRecent(vm.page).then(function () {
                vm.disableShowMore = false;
                vm.buttonMessage = 'Load More';
            });
        };

        vm.getRecent();

        function syncIdeas() {
            vm.ideas = angular.copy(IdeasService.ideas);
        }


        vm.authenticate = function (provider) {
            UsersService.authenticate(provider);
        };



    }

})();