(function () {

    'use strict';

    angular.module('willitspark').controller('ShowIdeaController', ShowIdeaController);

    function ShowIdeaController($auth, $state, $stateParams, $rootScope, BaseService, IdeaFeedbackService, IdeasService, $scope, $uibModal, Analytics) {

        var vm = this;
        $scope.idea = {};
        vm.others = [];
        vm.feedback = [];
        vm.tags = [];

        $scope.$watch(function () {
            return IdeasService.idea;
        },
                function (newValue, oldValue) {
                    syncIdea();
                }, true);

        $scope.$watch(function () {
            return IdeasService.others;
        },
                function (newValue, oldValue) {
                    syncOthers();
                }, true);

        $scope.$watch(function () {
            return IdeaFeedbackService.feedback;
        },
                function (newValue, oldValue) {
                    syncFeedback();
                }, true);

        $scope.$watch(function () {
            return IdeaFeedbackService.tags;
        },
                function (newValue, oldValue) {
                    syncTags();
                }, true);


        vm.show = function () {
            var id = $stateParams.id;
            IdeasService.show({id: id}).then(function (data) {
                IdeasService.getOthers({id: data.data.idea.id});
                IdeaFeedbackService.indexByTags({id: data.data.idea.id});
                IdeaFeedbackService.index({id: data.data.idea.id});
            });
        };

        vm.show();

        function syncIdea() {
            $scope.idea = angular.copy(IdeasService.idea);
        }

        function syncOthers() {
            vm.others = angular.copy(IdeasService.others);
        }

        function syncFeedback() {
            vm.feedback = angular.copy(IdeaFeedbackService.feedback);
        }

        function syncTags() {
            vm.tags = angular.copy(IdeaFeedbackService.tags);
        }

        vm.rate = function (idea, stat, rating) {
            if (!$rootScope.authenticated) {
                vm.openLogin();
            } else {
                IdeaFeedbackService.postRate(idea, stat, rating);
            }
        };

        vm.deleteRate = function (idea, stat) {
            IdeaFeedbackService.deleteRate(idea, stat);
        };

        vm.visit = function (idea) {
            Analytics.trackEvent('ideas', 'visit', idea.id);
        };

        vm.createFeedback = function (idea, stat) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'js/angular/shared/ideas/_feedbackBox.html',
                resolve: {
                    idea: function () {
                        return idea;
                    },
                    stat: function () {
                        return stat;
                    }
                },
                controller: function ($scope, idea, stat, IdeaFeedbackService, $uibModalInstance) {
                    $scope.idea = idea;
                    $scope.stat = stat;
                    $scope.feedback = '';
                    $scope.postFeedback = function (idea, stat, feedback) {
                        if (feedback != '') {
                            IdeaFeedbackService.postFeedback(idea, stat, feedback).then(function () {
                                $uibModalInstance.dismiss();
                            });
                        }
                    };
                    $scope.closeBox = function () {
                        $uibModalInstance.dismiss();
                    };
                },
                backdrop: true,
                windowClass: 'feedback-box-modal'
            });
        };

        vm.openLogin = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'js/angular/components/login/login.html',
                controller: 'LoginController',
                controllerAs: 'LoginCtrl',
                backdrop: true,
                windowClass: 'login-modal'
            });

        };

    }

})();