(function () {

    'use strict';

    angular.module('willitspark').controller('MyIdeasController', MyIdeasController);

    function MyIdeasController($scope, IdeasService) {

        var vm = this;
        vm.ideas = [];

        $scope.$watch(function () {
            return IdeasService.ideas;
        },
                function (newValue, oldValue) {
                    syncIdeas();
                }, true);

        vm.getMyIdeas = function () {
            IdeasService.getMyIdeas();
        };


        vm.getMyIdeas();


        function syncIdeas() {
            vm.ideas = angular.copy(IdeasService.ideas);
        }



    }

})();