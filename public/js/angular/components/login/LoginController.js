(function () {

    'use strict';

    angular.module('willitspark').controller('LoginController', LoginController);

    function LoginController($auth, $state, $http, $rootScope, BaseService, $uibModalInstance, UsersService, $window) {

        var vm = this;
        vm.error = {};

        vm.authenticate = function (provider) {
            UsersService.authenticate(provider).then(function () {
                $uibModalInstance.dismiss();
                //if condition to perserve input info for creating and editing startup

            });
        };




    }

})();