(function () {

    'use strict';

    angular.module('willitspark').controller('MyFavoritesController', MyFavoritesController);

    function MyFavoritesController(IdeasService, $scope) {

        var vm = this;
        vm.ideas = [];

        $scope.$watch(function () {
            return IdeasService.ideas;
        },
                function (newValue, oldValue) {
                    syncIdeas();
                }, true);

        vm.getMyFavorites = function () {
            IdeasService.getMyFavorites();
        };


        vm.getMyFavorites();


        function syncIdeas() {
            vm.ideas = angular.copy(IdeasService.ideas);
        }






    }

})();