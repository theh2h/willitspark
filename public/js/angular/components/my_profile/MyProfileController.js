(function () {

    'use strict';

    angular.module('willitspark').controller('MyProfileController', MyProfileController);

    function MyProfileController($scope, UsersService) {

        var vm = this;
        vm.user = {};
        vm.error = {};


        $scope.$watch(function () {
            return UsersService.user;
        },
                function (newValue, oldValue) {
                    syncIdeas();
                }, true);

        vm.getMyProfile = function () {
            UsersService.getMyProfile();
        };


        vm.getMyProfile();


        function syncIdeas() {
            vm.user = angular.copy(UsersService.user);
        }




    }

})();