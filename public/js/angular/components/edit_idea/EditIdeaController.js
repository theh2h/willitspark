(function () {

    'use strict';

    angular.module('willitspark').controller('EditIdeaController', EditIdeaController);

    function EditIdeaController($state, IdeasService, $rootScope, $uibModal, $stateParams) {

        var vm = this;
        vm.idea = {
            logo: null,
            name: '',
            s_description: '',
            solution: '',
            tags: [],
            idea_stage_id: null
        };
        vm.tags = [];
        vm.uploading = false;
        vm.error = {};

        edit();

        vm.update = function (idea) {
            if (!$rootScope.authenticated) {
                vm.openLogin();
            } else {
                vm.uploading = true;
                IdeasService.update(idea).then(function (data) {
                    $state.go('show', {id: data.data.idea.id});
                });
            }

        };

        vm.openLogin = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'js/angular/components/login/login.html',
                controller: 'LoginController',
                controllerAs: 'LoginCtrl',
                backdrop: true,
                windowClass: 'login-modal'
            });

        };

        function edit() {
            var id = $stateParams.id;
            IdeasService.edit({id: id}).then(function () {
                vm.tags = angular.copy(IdeasService.editObj.tags);
                vm.stages = angular.copy(IdeasService.editObj.stages);
                vm.idea = angular.copy(IdeasService.editObj.idea);
            });
        }




    }

})();