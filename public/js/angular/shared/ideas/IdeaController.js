(function () {

    'use strict';

    angular.module('willitspark').controller('IdeaController', IdeaController);

    function IdeaController($scope, $rootScope, IdeasService, $uibModal, $state, Analytics) {

        var vm = this;
        vm.showMoreCardBoolean = $state.is('show') ? true : false;



        vm.heart = function (idea) {
            if (!$rootScope.authenticated) {
                vm.openLogin();
            } else {
                IdeasService.heart(idea);
            }
        };

        vm.unheart = function (idea) {
            IdeasService.unheart(idea);
        };

        vm.yesFunction = function (idea) {
            if (!$rootScope.authenticated) {
                vm.openLogin();
            } else {
                IdeasService.yesFunction(idea);
            }
        };

        vm.noFunction = function (idea) {
            if (!$rootScope.authenticated) {
                vm.openLogin();
            } else {
                IdeasService.noFunction(idea);
            }
        };

        vm.share = function (idea, provider) {
            Analytics.trackEvent('ideas', 'share', provider);
        };



        vm.deleteYesNo = function (idea) {
            IdeasService.deleteYesNo(idea);

        };

        vm.showMoreCard = function () {
            vm.showMoreCardBoolean = !vm.showMoreCardBoolean;
        };

        vm.openLogin = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'js/angular/components/login/login.html',
                controller: 'LoginController',
                controllerAs: 'LoginCtrl',
                backdrop: true,
                windowClass: 'login-modal'
            });

        };

    }

})();