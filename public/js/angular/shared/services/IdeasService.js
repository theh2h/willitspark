(function () {

    angular.module('willitspark').factory('IdeasService', ['BaseService', '$http', '$rootScope', '$auth', 'Upload', 'Analytics', function (BaseService, $http, $rootScope, $auth, Upload, Analytics) {

            var service = {};
            var url = 'api/ideas';
            service.ideas = [];
            service.idea = {};
            service.createObj = {
                tags: []
            };
            service.editObj = {
                tags: []
            };
            service.others = [];
            service.error = {};

            service.getOthers = function (idea) {
                BaseService.load();
                return $http.get(url + '/' + idea.id + '/others')
                        .success(function (data) {
                            service.others = data.ideas;
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            service.getMyFavorites = function () {
                BaseService.load();
                return $http.get(url + '/my/favorites')
                        .success(function (data) {
                            service.ideas = data.ideas;
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            service.getMyIdeas = function () {
                BaseService.load();
                return $http.get(url + '/my/ideas')
                        .success(function (data) {
                            service.ideas = data.ideas;
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            service.getFeatured = function () {
                BaseService.load();
                return $http.get(url + '/featured')
                        .success(function (data) {
                            service.ideas = data.ideas;
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            service.getTop = function (page) {
                BaseService.load();
                return $http.get(url + '/top?page=' + page)
                        .success(function (data) {
                            if (page == 1) {
                                service.ideas = [];
                            }
                            service.ideas.push.apply(service.ideas, data.ideas.data);
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            service.getViews = function (page) {
                BaseService.load();
                return $http.get(url + '/views?page=' + page)
                        .success(function (data) {
                            if (page == 1) {
                                service.ideas = [];
                            }
                            service.ideas.push.apply(service.ideas, data.ideas.data);
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            service.getRecent = function (page) {
                BaseService.load();
                return $http.get(url + '/recent?page=' + page)
                        .success(function (data) {
                            if (page == 1) {
                                service.ideas = [];
                            }
                            service.ideas.push.apply(service.ideas, data.ideas.data);
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            service.create = function () {
                BaseService.load();
                return $http.get(url + '/create')
                        .success(function (data) {
                            service.createObj.tags = data.tags;
                            service.createObj.stages = data.stages;
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            service.edit = function (idea) {
                BaseService.load();
                return $http.get(url + '/' + idea.id + '/edit')
                        .success(function (data) {
                            service.editObj.idea = data.idea;
                            service.editObj.tags = data.tags;
                            service.editObj.stages = data.stages;
                            angular.forEach(service.editObj.idea.interests, function (value, key) {
                                var index = findInArray(service.editObj.tags, value.id);
                                service.editObj.tags[index].ticked = true;
                            });

                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            service.store = function (idea) {
                BaseService.load();

                if (idea.tags.length > 0) {
                    if (idea.tags[0] === Object(idea.tags[0])) {
                        var temp = idea.tags;
                        idea.tags = [];
                        angular.forEach(temp, function (value, key) {
                            idea.tags.push(value.id);
                        });
                    }
                }

                return  Upload.upload({
                    url: url,
                    data: {file: idea.logo,
                        name: idea.name,
                        s_description: idea.s_description,
                        solution: idea.solution,
                        url: idea.url,
                        tags: idea.tags,
                        idea_stage_id: idea.idea_stage_id
                    },
                }).success(function (data) {

                })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };
            service.show = function (idea) {
                BaseService.load();
                return $http.get(url + '/' + idea.id)
                        .success(function (data) {

                            service.idea = data.idea;
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };
            service.update = function (idea) {
                BaseService.load();
                idea.tags = [];
                idea.tags = idea.interests;


                if (idea.tags.length > 0) {
                    if (idea.tags[0] === Object(idea.tags[0])) {
                        var temp = idea.tags;
                        idea.tags = [];
                        angular.forEach(temp, function (value, key) {
                            idea.tags.push(value.id);
                        });
                    }
                }


                return  Upload.upload({
                    url: url + '/' + idea.id,
                    method: 'POST',
                    data: {
                        _method: 'PATCH',
                        file: idea.logo,
                        name: idea.name,
                        s_description: idea.s_description,
                        solution: idea.solution,
                        url: idea.url,
                        tags: idea.tags,
                        idea_stage_id: idea.idea_stage_id
                    },
                })
                        .success(function (data) {

                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            service.heart = function (idea) {
                BaseService.load();
                return $http.get(url + '/' + idea.id + '/heart')
                        .success(function (data) {
                            Analytics.trackEvent('ideas', 'heart', idea.id);
                            service.ideas[findInArray(service.ideas, idea.id)] = data.idea;
                            if (service.idea !== null) {
                                if (service.idea.id == idea.id) {
                                    service.idea = data.idea;
                                }
                            }

                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            service.unheart = function (idea) {
                BaseService.load();
                return $http.get(url + '/' + idea.id + '/unheart')
                        .success(function (data) {
                            service.ideas[findInArray(service.ideas, idea.id)] = data.idea;
                            if (service.idea !== null) {
                                if (service.idea.id == idea.id) {
                                    service.idea = data.idea;
                                }
                            }
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            service.yesFunction = function (idea) {
                BaseService.load();
                return $http.get(url + '/' + idea.id + '/yes')
                        .success(function (data) {
                            Analytics.trackEvent('ideas', 'yes', idea.id);
                            if (service.idea !== null) {
                                if (service.idea.id == idea.id) {
                                    service.idea = data.idea;
                                }
                            }
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };
            service.noFunction = function (idea) {
                BaseService.load();
                return $http.get(url + '/' + idea.id + '/no')
                        .success(function (data) {
                            Analytics.trackEvent('ideas', 'no', idea.id);
                            if (service.idea !== null) {
                                if (service.idea.id == idea.id) {
                                    service.idea = data.idea;
                                }
                            }
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };


            service.deleteYesNo = function (idea) {
                BaseService.load();
                return $http.delete(url + '/' + idea.id + '/undoyesno')
                        .success(function (data) {
                            if (service.idea !== null) {
                                if (service.idea.id == idea.id) {
                                    service.idea = data.idea;
                                }
                            }
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };



            function findInArray(arraytosearch, valuetosearch) {
                for (var i = 0; i < arraytosearch.length; i++) {
                    if (arraytosearch[i].id == valuetosearch) {
                        return i;
                    }
                }
                return null;
            }

            return service;
        }]);
})();