(function () {

    angular.module('willitspark').factory('UsersService', ['BaseService', '$http', '$rootScope', '$auth', '$window', '$state', 'Analytics', function (BaseService, $http, $rootScope, $auth, $window, $state, Analytics) {

            var service = {};
            var url = 'api/users';
            service.user = {};
            service.error = {};


            service.authenticate = function (provider) {
                return $auth.authenticate(provider).then(function (data) {
                    var user = JSON.stringify(data.data.user);

                    // Set the stringified user data into local storage
                    localStorage.setItem('user', user);

                    // The user's authenticated state gets flipped to
                    // true so we can now show parts of the UI that rely
                    // on the user being logged in
                    $rootScope.authenticated = true;


                    // Putting the user's data on $rootScope allows
                    // us to access it anywhere across the app
                    $rootScope.currentUser = data.data.user;

                    BaseService.flash('success', 'Hello , ' + $rootScope.currentUser.f_name);
                    Analytics.trackEvent('users', 'signin', provider);
                    if ($state.current.name !== 'new_idea' && $state.current.name !== 'edit_idea') {
                        $window.location.reload();
                    }
                });
            };

            service.getMyProfile = function () {
                BaseService.load();
                return $http.get(url + '/my/profile')
                        .success(function (data) {
                            service.user = data.user;
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            function findInArray(arraytosearch, valuetosearch) {
                for (var i = 0; i < arraytosearch.length; i++) {
                    if (arraytosearch[i].id == valuetosearch) {
                        return i;
                    }
                }
                return null;
            }

            return service;
        }]);
})();