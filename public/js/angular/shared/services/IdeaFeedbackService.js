(function () {

    angular.module('willitspark').factory('IdeaFeedbackService', ['BaseService', '$http', '$rootScope', '$auth', 'Upload', 'Analytics', function (BaseService, $http, $rootScope, $auth, Upload, Analytics) {

            var service = {};
            var url = 'api/ideas/';
            var url_extension = '/feedback';
            service.feedback = [];
            service.tags = [];
            service.feedbackObj = {};
            service.createObj = {
                tags: []
            };
            service.editObj = {
                tags: []
            };
            service.error = {};


            service.indexByTags = function (idea) {
                BaseService.load();
                return $http.get(url + idea.id + url_extension + '/tags')
                        .success(function (data) {
                            service.tags = data.tags;
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };
            service.index = function (idea) {
                BaseService.load();
                return $http.get(url + idea.id + url_extension)
                        .success(function (data) {
                            service.feedback = data.feedback;
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            service.create = function (idea) {
                BaseService.load();
                return $http.get(url + idea.id + url_extension + '/create')
                        .success(function (data) {
                            service.createObj.tags = data.tags;
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            service.store = function (idea, feedback) {
                BaseService.load();

                return $http.post(url + idea.id + url_extension, feedback)
                        .success(function (data) {
                            service.feedback.push(data.feedback);
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });


            };

            service.postRate = function (idea, stat, rating) {
                BaseService.load();
                return $http.post(url + idea.id + url_extension + '/tags/' + stat.id + '/rate', {rating: rating})
                        .success(function (data) {
                            Analytics.trackEvent('ideas', 'rate', idea.id);
                            service.indexByTags(idea);
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });


            };

            service.postFeedback = function (idea, stat, feedback) {
                BaseService.load();
                return $http.post(url + idea.id + url_extension + '/tags/' + stat.id + '/feedback', {feedback: feedback})
                        .success(function (data) {
                            Analytics.trackEvent('ideas', 'feedback', stat.name);
                            service.index(idea);
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });
            };

            service.deleteRate = function (idea, stat) {
                BaseService.load();

                return $http.delete(url + idea.id + url_extension + '/tags/' + stat.id + '/rate')
                        .success(function (data) {
                            service.indexByTags(idea);
                            service.index(idea);
                        })
                        .error(function (error) {
                            service.error = error;
                        })
                        .finally(function () {
                            BaseService.unload();
                        });


            };

            function findInArray(arraytosearch, valuetosearch) {
                for (var i = 0; i < arraytosearch.length; i++) {
                    if (arraytosearch[i].id == valuetosearch) {
                        return i;
                    }
                }
                return null;
            }

            return service;
        }]);
})();