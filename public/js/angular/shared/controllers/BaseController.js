(function () {

    'use strict';

    angular.module('willitspark').controller('BaseController', BaseController);

    function BaseController($auth, $state, $stateParams, $rootScope, BaseService, $window, UsersService) {

        var vm = this;

        $rootScope.$state = $state;

        vm.windowHeight = ($window.innerHeight) + 'px';

        vm.feedbackOptions = {
            initButtonText: 'Send Us Your Opinion',
            postTimeStamp: true,
            postBrowserInfo: true,
            postHTML: false,
            postURL: true,
            screenshotStroke: false,
            highlightElement: false,
            ajaxURL: window.location.origin + '/api/feedback'
//            showDescriptionModal: false
        };

        vm.isActive = function (viewLocation) {
            return viewLocation === $state.current.name;
        };

        BaseService.unload();

        vm.authenticate = function (provider) {
            UsersService.authenticate(provider).then(function () {

            });
        };

        vm.logout = function () {
            $auth.logout().then(function () {

                // Remove the authenticated user from local storage
                localStorage.removeItem('user');

                // Flip authenticated to false so that we no longer
                // show UI elements dependant on the user being logged in
                $rootScope.authenticated = false;

                // Remove the current user info from rootscope
                $rootScope.currentUser = null;
            });
        };

    }

})();