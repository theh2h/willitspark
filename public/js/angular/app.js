(function () {

    'use strict';
    angular
            .module('willitspark', ['ui.router', 'ngStorage', 'ui.bootstrap', 'satellizer', 'ngAnimate', 'flash', '720kb.socialshare', 'angular-google-analytics', 'ngFileUpload', 'angular-svg-round-progressbar', 'angular-inview', 'isteven-multi-select', 'angular-send-feedback'])
            .config(function ($locationProvider, $stateProvider, $urlRouterProvider, $authProvider, $httpProvider, $provide, AnalyticsProvider) {

                AnalyticsProvider.setAccount('UA-81036067-1');
                AnalyticsProvider.setDomainName('http://www.willitspark.com');
//                AnalyticsProvider.logAllCalls(true).startOffline(true);
                AnalyticsProvider.ignoreFirstPageLoad(true);

                AnalyticsProvider.setPageEvent('$stateChangeSuccess');



                function redirectWhenLoggedOut($q, $injector, $rootScope) {

                    return {
                        responseError: function (rejection) {

                            // Need to use $injector.get to bring in $state or else we get
                            // a circular dependency error
                            var $state = $injector.get('$state');
                            // Instead of checking for a status code of 400 which might be used
                            // for other reasons in Laravel, we check for the specific rejection
                            // reasons to tell us if we need to redirect to the login state
                            var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid'];
                            // Loop through each rejection reason and redirect to the login
                            // state if one is encountered
                            angular.forEach(rejectionReasons, function (value, key) {

                                if (rejection.data.error === value) {

                                    // If we get a rejection corresponding to one of the reasons
                                    // in our array, we know we need to authenticate the user so 
                                    // we can remove the current user from local storage
                                    // Remove the authenticated user from local storage
                                    localStorage.removeItem('user');

                                    // Flip authenticated to false so that we no longer
                                    // show UI elements dependant on the user being logged in
                                    $rootScope.authenticated = false;

                                    // Remove the current user info from rootscope
                                    $rootScope.currentUser = null;
//                                    $state.go('home');

                                }
                            });
                            return $q.reject(rejection);
                        }
                    };
                }
                // LinkedIn
                $authProvider.linkedin({
                    clientId: '774jcvvjcxzils',
                    url: 'api/users/authenticate/linkedin',
                    authorizationEndpoint: 'https://www.linkedin.com/uas/oauth2/authorization',
                    redirectUri: window.location.origin + '/' + 'willitspark/public/',
                    requiredUrlParams: ['state'],
                    scope: ['r_emailaddress', 'r_fullprofile'],
                    scopeDelimiter: ' ',
                    state: 'STATE',
                    type: '2.0',
                    popupOptions: {width: 527, height: 582}
                });
                // Google
                $authProvider.google({
                    clientId: '904286041169-6k0298k8pmtd11ktotv8mb3vr5tqpjm0.apps.googleusercontent.com',
                    url: 'api/users/authenticate/google',
                    authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
                    redirectUri: window.location.origin + '/' + 'willitspark/public/',
                    requiredUrlParams: ['scope'],
                    optionalUrlParams: ['display'],
                    scope: ['profile', 'email'],
                    scopePrefix: 'openid',
                    scopeDelimiter: ' ',
                    display: 'popup',
                    oauthType: '2.0',
                    popupOptions: {width: 452, height: 633}
                });

                // Setup for the $httpInterceptor
                $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);
                // Push the new factory onto the $http interceptor array
                $httpProvider.interceptors.push('redirectWhenLoggedOut');
                // Satellizer configuration that specifies which API
                // route the JWT should be retrieved from
                $authProvider.baseUrl = null;
                //$authProvider.httpInterceptor = true;
                $authProvider.loginUrl = 'api/users/authenticate';
                $authProvider.loginRedirect = null;
                // Redirect to the auth state if any other states
                // are requested other than users
                $urlRouterProvider.otherwise('/');
                $locationProvider.html5Mode(true);

                $stateProvider

                        .state('home', {
                            url: '/',
                            templateUrl: 'js/angular/components/home/index.html',
                            controller: 'HomeController as HomeCtrl'
                        })
                        .state('browse', {
                            url: '/ideas',
                            templateUrl: 'js/angular/components/browse/index.html',
                            controller: 'BrowseController as BrowseCtrl'
                        })
                        .state('new_idea', {
                            url: '/idea/new',
                            templateUrl: 'js/angular/components/postNewIdea/index.html',
                            controller: 'PostNewIdeaController as PostNewIdeaCtrl'
                        })
                        .state('show', {
                            url: '/idea/:id',
                            templateUrl: 'js/angular/components/idea/index.html',
                            controller: 'ShowIdeaController as ShowIdeaCtrl'
                        })
                        .state('my_ideas', {
                            url: '/my/ideas',
                            templateUrl: 'js/angular/components/my_ideas/index.html',
                            controller: 'MyIdeasController as MyIdeasCtrl'
                        })
                        .state('my_favorites', {
                            url: '/my/favorites',
                            templateUrl: 'js/angular/components/my_favorites/index.html',
                            controller: 'MyFavoritesController as MyFavoritesCtrl'
                        })
                        .state('my_profile', {
                            url: '/my/profile',
                            templateUrl: 'js/angular/components/my_profile/index.html',
                            controller: 'MyProfileController as MyProfileCtrl'
                        })
                        .state('edit_idea', {
                            url: '/idea/:id/edit',
                            templateUrl: 'js/angular/components/edit_idea/index.html',
                            controller: 'EditIdeaController as EditIdeaCtrl'
                        })
                        .state('how_it_works', {
                            url: '/about',
                            templateUrl: 'js/angular/components/how_it_works/index.html',
                            controller: 'HowItWorksController as HowItWorksCtrl'
                        })

                        ;

            })
            .filter('num', function () {
                return function (input) {
                    return parseInt(input, 10);
                };
            })
            .filter('datetimeToNow', function () {
                return function (input) {
                    if (!input)
                        return;

                    return moment(input).fromNow();
                };
            })
            .directive('ngReallyClick', ['$modal',
                function ($modal) {
                    var ModalInstanceCtrl = function ($scope, $modalInstance) {
                        $scope.ok = function () {
                            $modalInstance.close();
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    };
                    return {
                        restrict: 'A',
                        scope: {
                            ngReallyClick: "&",
                            item: "="
                        },
                        link: function (scope, element, attrs) {
                            element.bind('click', function () {
                                var message = attrs.ngReallyMessage || "Are you sure ?";

                                var modalHtml = '<div class="modal-body">' + message + '</div>';
                                modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';
                                var modalInstance = $modal.open({
                                    template: modalHtml,
                                    controller: ModalInstanceCtrl
                                });
                                modalInstance.result.then(function () {
                                    scope.ngReallyClick({item: scope.item}); //raise an error : $digest already in progress
                                }, function () {
                                    //Modal dismissed
                                });
                                //*/

                            });
                        }
                    };
                }
            ])

            .run(function ($rootScope, $state, $window, $location, Analytics) {
//                if (window.location.href.indexOf('localhost') < 0) {
//                    $window.ga('create', 'UA-81036067-1', 'auto');
//
//                    $rootScope.$on('$stateChangeSuccess', function (event) {
//                        $window.ga('send', 'pageview', $window.location.href);
//                    });
//                }





                // $stateChangeStart is fired whenever the state changes. We can use some parameters
                // such as toState to hook into details about the state as it is changing
                $rootScope.$on('$stateChangeStart', function (event, toState) {
                    //scroll to top automatically

                    document.body.scrollTop = document.documentElement.scrollTop = 0;

                    // Grab the user from local storage and parse it to an object
                    var user = JSON.parse(localStorage.getItem('user'));
//                    var allowed_states_for_guest = ['login'];
                    // If there is any user data in local storage then the user is quite
                    // likely authenticated. If their token is expired, or if they are
                    // otherwise not actually authenticated, they will be redirected to
                    // the auth state because of the rejected request anyway


                    if (user) {

                        // The user's authenticated state gets flipped to
                        // true so we can now show parts of the UI that rely
                        // on the user being logged in
                        $rootScope.authenticated = true;
                        // Putting the user's data on $rootScope allows
                        // us to access it anywhere across the app. Here
                        // we are grabbing what is in local storage
                        $rootScope.currentUser = user;



                    } else {
                        $rootScope.authenticated = false;
                        $rootScope.currentUser = null;

                    }
                });

            });
})();