var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix
            .styles([
                '../bower_components/animate.css/animate.min.css',
                '../bower_components/font-awesome/css/font-awesome.css',
                '../bower_components/bootstrap-social/bootstrap-social.css',
                '../bower_components/isteven-angular-multiselect/isteven-multi-select.css',
                '../bower_components/angular-send-feedback/dist/angular-send-feedback.min.css'
            ], "public/css/pure/")
            .sass('app.scss', 'public/css', {includePaths: ['resources/assets/bower_components/bootstrap-sass/assets/stylesheets/']})
            .scripts([
                '../bower_components/jquery/dist/jquery.min.js',
                '../bower_components/angular/angular.js',
                '../bower_components/ngstorage/ngStorage.min.js',
                '../bower_components/angular-ui-router/release/angular-ui-router.min.js',
                '../bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
                '../bower_components/moment/moment.js',
                '../bower_components/ng-file-upload/ng-file-upload.min.js',
                '../bower_components/ng-file-upload/ng-file-upload-shim.min.js',
                '../bower_components/isteven-angular-multiselect/isteven-multi-select.js',
                '../bower_components/angular-animate/angular-animate.min.js',
                '../bower_components/angular-flash-alert/dist/angular-flash.min.js',
                '../bower_components/satellizer/satellizer.js',
                '../bower_components/angular-socialshare/dist/angular-socialshare.js',
                '../bower_components/html2canvas/build/html2canvas.min.js',
                '../bower_components/angular-send-feedback/dist/angular-send-feedback.min.js',
                '../bower_components/angular-inview/angular-inview.js',
                '../bower_components/angular-svg-round-progressbar/build/roundProgress.js',
                '../bower_components/angular-google-analytics/dist/angular-google-analytics.js'

            ], 'public/js/app.js')

//            .copy('bower_components/bootstrap-sass/assets/fonts/bootstrap/**', 'public/fonts')
            .copy('resources/assets/bower_components/font-awesome/fonts/**', 'public/css/fonts')
            .copy('resources/assets/bower_components/bootstrap-sass/assets/fonts/**', 'public/fonts');



});
